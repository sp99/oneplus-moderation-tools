// ==UserScript==
// @name         Forum Moderation Tools
// @namespace    *.oneplus.net*
// @version      1.0.2
// @description  Mango Bananas
// @author       Kevin Pei aka kp1234, Sam Prescott aka sp99
// @include      *forums.oneplus.net*
// @grant        none
// @license      Copyright (C) 2014 OnePlus Moderation Team - All Rights Reserved | Unauthorized copying and/or use of this file, via any medium is strictly prohibited
// ==/UserScript==
//MAKE SURE TO UPDATE THIS NUMBER
if(typeof GM_info === 'undefined')
    fMTVersion = "1.0.2";
else
    fMTVersion = GM_info.script.version;

function addJQuery(callback) {
    $('<style type="text/css"></style').text(".InlineModChecked, .InlineModChecked .posterAvatar, .InlineModChecked .main, .InlineModChecked .stats, .InlineModChecked .lastPost, .deleted.InlineModChecked, .moderated.InlineModChecked {background: rgb(255,255,200) url('/styles/oneplus2014/xenforo/gradients/category-23px-light.png') repeat-x top;}#modbar{background:#FFF;box-shadow:0px 0px 10px rgba(0,0,0,0.2);position:fixed;bottom:0px;left:0px;width:100%;z-index:100;}.modbar-btn{cursor:pointer;display:inline-block;padding:15px;font-size:14px;color:#666;transition:all 200ms;}.modbar-btn:hover,.modbar-btn.active{background:#EEE;}.modbar-overlay{background:#FFF;box-shadow:0px 0px 10px rgba(0,0,0,0.2);position:fixed;z-index:99;overflow:auto;max-height:500px;}.modbar-overlay-section{white-space: nowrap;padding:15px 10px;color:#666;font-weight:bold;border-bottom:1px solid #EEE;overflow: hidden;text-overflow: ellipsis;}").appendTo('head');
    var script = document.createElement("script");
    script.textContent = "(" + callback.toString() + ")();";
    document.body.appendChild(script);
}
function main() {
	var docCookies = {
	  getItem: function (sKey) {
		if (!sKey) { return null; }
		return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
	  },
	  setItem: function (sKey, sValue, vEnd, sPath, sDomain, bSecure) {
		if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return false; }
		var sExpires = "";
		if (vEnd) {
		  switch (vEnd.constructor) {
			case Number:
			  sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
			  break;
			case String:
			  sExpires = "; expires=" + vEnd;
			  break;
			case Date:
			  sExpires = "; expires=" + vEnd.toUTCString();
			  break;
		  }
		}
		document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
		return true;
	  },
	  removeItem: function (sKey, sPath, sDomain) {
		if (!this.hasItem(sKey)) { return false; }
		document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "");
		return true;
	  },
	  hasItem: function (sKey) {
		if (!sKey) { return false; }
		return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
	  },
	  keys: function () {
		var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
		for (var nLen = aKeys.length, nIdx = 0; nIdx < nLen; nIdx++) { aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]); }
		return aKeys;
	  }
	};
    
    function fMTUpdate(manual) {
            var re;
            $.ajax({
                type : 'GET',
                url : 'https://forums.oneplus.net/threads/oneplus-moderation-tools-easy-to-install-script.182356/#post-11332892',
                success : function (data) {
                    var ver2 = data.match(/\d\.\d\.\d/i);
                    var v1 = fMTVersion.toString().split(".");
                    var v2 = ver2[0].split(".");
                    console.log(v1);
                    console.log(v2);
                    for(y=0;y<v2.length;y++){
                        v2[y] = parseInt(v2[y]);
                        v1[y] = parseInt(v1[y]);
                    }
                    if (v1[0] < v2[0] || v1[1] < v2[1] || v1[2] < v2[2]) {
                        var updateText = "New version found! \nWould you like to view the release page and update?";
                        new modal('Update!', updateText, {
                            'Yes': {
                                type: 'red',
                                click: function(){
                                    this.close();
                                    location.href="https://forums.oneplus.net/threads/oneplus-moderation-tools-easy-to-install-script.182356/";
                                    setTimeout(function(){
                                        location.reload(true);
                                    }, 1000);
                                }
                            },
                            'Not Now': {
                                type: 'grey',
                                click: function(){
                                    this.close();
                                }
                            }
                        });
                    }else{
                        if(manual){
                            new modal('No New Updates Found', updateText, {
                                'Ok': {
                                    type: 'red',
                                    click: function(){
                                        this.close();
                                    }
                                }
                            });
                        }
                    }
                }
            });
        }
    
    function modal(title, content, btns){
        var overlayObj = $('<div style="position: fixed;margin: auto;top: 0;left: 0;width: 100%;height: 100%;z-index: 209998;opacity: 0.9;filter: alpha(opacity=90);background-color: rgb(255,255,255);"></div>');
        var modalObj = $('<div class="xenOverlay" style="display: block;position: fixed;left: 50%;width: 600px;z-index:209999;margin-left: -300px;top: 50%;height: auto;"><form class="formOverlay xenForm animateClose"><div class="heading" id="redactor_modal_header">'+title+'</div><div id="redactor_modal_inner"><dl class="ctrlUnit"><div class="modal-inner-content"></div></dl><dl class="ctrlUnit submitUnit modal-btn-wrapper"></dl></div></form></div>');
        modalObj.find('.modal-inner-content').append(content);
        var modalMethods = {
            close: function(){
                modalObj.find('.xenForm').removeClass('open').delay(300).hide(1, function(){
					modalObj.remove();
				});
                overlayObj.fadeOut(300, function(){
					overlayObj.remove();
				});
            },
            add: function(data){
                modalObj.find('.modal-inner-content').append(data);
            }
        };
        this.methods = modalMethods;
        $.each(btns, function(index, value) {
            var btn = $('<button class="redactor_modal_btn button" style="margin-right:5px;">'+index+'</button>');
            if(value.type == "red"){
                btn.addClass('primary');
            }
            modalObj.find('.modal-btn-wrapper').append(btn);
            btn.click(function(e){
                e.preventDefault();
                btns[index].click.call(modalMethods);
            });
        });
        modalObj.appendTo('body');
        modalObj.css('margin-top', -modalObj.outerHeight()/2);
        overlayObj.hide().appendTo('body').fadeIn(300);
		modalObj.find('.xenForm').addClass('open');
    }
	
    function closeThread(batch, reason, custom) {
		if(typeof custom == 'undefined' || custom == ''){
			custom = '';
		}
        function getWarningMsg(option, name, title) {
			if(typeof custom == 'undefined' || custom == ''){
				custom = '';
			}else{
				custom = custom.replace(/(?:\r\n|\r|\n)/g, '<br />');
				custom = '<br/><p>'+custom+'</p>';
			}
            if (option == 1) {
                return '<p>Hi there @' + name + '!</p><br/><p>It has come to my attention that you have created a thread to ask for invites, however that is not allowed.</p><br/><p>The best way to receive an invite is to hang around the forums, join in on the community discussion and create quality replies and threads.</p>'+custom+'<br/><p>If you have any further questions, please read the following thread https://forums.oneplus.net/threads/2-rules-revisited-and-restated.78159/, or feel free to message me via the private message feature.</p><br/><p>Thanks for your understanding!</p><br/><p>Thread closed.</p>';
            }
            if (option == 2) {
                var link = prompt("Duplicate link for " + title + "? (enter 0 to skip this)");
                return '<p>Hi there @' + name + '!</p><br/><p>It appears that this has already been discussed ' + (link ? ('at ' + link + '.</p>') : 'before.') + custom+'<br/><p>Please search in the future prior to creating a thread, so we don\'t encounter any duplicates :).</p><br/><p>If you have any further questions, feel free to message me via the private message feature.</p><br/><p>Thanks for your understanding!</p><br/><p>Thread closed.</p>';
            }
            if (option == 3) {
                return '<p>Hi there @' + name + '!</p><br/><p>It has come to my attention that you have linked to a scalper site, however that is not allowed.</p><br/><p>OnePlus is the one and only official retailer of the OnePlus One at the moment, and buying from other sources puts people at risk of scamming and other dangers.</p>'+custom+'<br/><p>If you have any further questions, please read the following thread https://forums.oneplus.net/threads/5-reasons-to-not-buy-the-oneplus-one-from-scalpers.35689/, or feel free to message me via the private message feature.</p><br/><p>Thanks for your understanding!</p>';
            }
            if (option == 4) {
                return '<p>Congrats to the winner(s)!</p><br/><p>Thank you @' + name + ' for sharing with the OnePlus community :)</p>'+custom+'<br/>Invites given, thread closed.</p>';
            }
        }
        function closePost(url, message, modalObj) {
            $.get(url, function(data) {
                var token = data.match(/_csrfToken: \"(.*)\"/)[1];
                var dataObject = $(data);
                var name = dataObject.find('.message:first').attr('data-author');
                var msg = getWarningMsg(message, name, dataObject.find('.titleBar:first h1').text());
                $.post(url + '/add-reply', {
                    message_html: msg,
                    _xfRelativeResolver: url,
                    last_date: Date.now(),
                    last_known_date: Date.now(),
                    _xfToken: token,
                    _xfRequestUri: url.replace("https://forums.oneplus.net", ""),
                    _xfNoRedirect: 1,
                    _xfResponseType: "json"
                }, function(data) {
                    if (data.error) {
                        alert(data.error[0]);
                        return 0;
                    }
                    $.post(url + '/quick-update', {
                        'set[discussion_open]': 1,
                        'set[sticky]': 1,
                        _xfToken: token,
                        _xfRequestUri: url.replace("https://forums.oneplus.net", ""),
                        _xfNoRedirect: 1,
                        _xfResponseType: 'json'
                    }, function(data) {
                        if (batch) {
                            modalObj.add('<li>Closed ' + (url.replace('https://forums.oneplus.net/threads/', '').replace('/', '').replace('-', ' ')) + '. Waiting 30 seconds...</li>');
                        } else {
                            location.reload(1);
                        }
                    });
                });
            });
        }
        function runClose(links, modalObj) {
            window.queue = [];
            for (var i = 0; i < links.length; i++) {
                var message = reason;
                window.queue.push([links[i].replace(/page-\d+/, '').split("#")[0], message]);
            }
            var thisArgs = window.queue.shift();
            closePost(thisArgs[0], thisArgs[1], modalObj);
            if (window.queue.length != 0) {
                window.closeInterval = setInterval(function() {
                    thisArgs = window.queue.shift();
                    closePost(thisArgs[0], thisArgs[1], modalObj);
                    if (window.queue.length == 0) {
                        alert('All thread(s) successfully closed!');
                        modalObj.remove();
                        clearInterval(window.closeInterval);
                    }
                }, 35000);
            } else {
                if (batch) {
                    alert('All thread(s) successfully closed!');
                    modalObj.close();
                }
            }
        }
        if (batch) {
            var txtArea = $('<textarea id="postUrls" class="textCtrl" style="height: 100px;resize: none;display:block;width:100%;"></textarea>');
            var modalInner = $('<div>Links:</div>');
            modalInner.append(txtArea);
            modal('Batch Close Threads', modalInner, {
                'Close!': {
                    type: 'red',
                    click: function(){
                        runClose(txtArea.val().split("\n"), this);
                    }
                },
                'Cancel': {
                    type: 'grey',
                    click: function(){
                        this.close();
                    }
                }
            });
            modal.appendTo('body');
            modal.find('.redactor_btn_modal_close').click(function() {
                modal.remove();
            });
            modal.find('#redactor_insert_urls_btn').click(function() {
                modal.find('.button').remove();
                links = modal.find('#postUrls').val().split("\n");
                runClose();
            });
        } else {
            runClose([location.href]);
        }
    }
	var selectedThreads = function(){
		var barSection = new module('<span>Selected Threads&nbsp;</span>', 'right');
		var barSectionInner = $('<strong style="font-weight:bold;"></strong>');
		if(window.location.href.indexOf('forums.oneplus.net/search') != -1){
			var autoTicker = $('<input type="checkbox" />');
			autoTicker.insertAfter('.pageNavLinkGroup:first');
			autoTicker.after('&nbsp;<small>Select All</small>');
			autoTicker.change(function(){
				if(autoTicker.is(':checked')){
					for(a in checkboxes){
						for(box in checkboxes[a]){
							checkboxes[a][box].prop('checked', true).trigger('change');
						}
					}
				}else{
					for(a in checkboxes){
						for(box in checkboxes[a]){
							checkboxes[a][box].prop('checked', false).trigger('change');
						}
					}
				}
			});
			$('.searchResults').css('margin-top', '10px');
		}
		barSection.btn.append(barSectionInner);
		barSection.overlay.width('400px');
		var checkboxes = {};
		var checked = [];
		var titles = {};
		var getChecked = function(){
			checked = docCookies.getItem('xf_inlinemod_threads');
			checked = checked ? checked.split(',') : [];
			for (a in checked) {
				checked[a] = Number(checked[a]);
			}
		};
		var addOverlay = function(id){
			var entry = $('<a href="/threads/'+checked[a]+'" style="padding-right:30px;display:block;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">'+checked[a]+'</a>');
			var remover = $('<a href="javascript:void(0);" style="float:right;text-decoration:none;color:#666;margin-top:-22px;position:relative;">x</a>');
			var entryOuter = $('<div class="modbar-overlay-section"></div>');
			entryOuter.append(entry).append(remover);
			barSection.overlay.append(entryOuter);
			remover.click(function(){
				checked.splice(checked.indexOf(id), 1);
				entryOuter.remove();
				digest();
				if(checked.length == 0){
					barSection.overlay.append('<div class="modbar-overlay-section"><em>No Threads Selected</em></div>');
				}
			});
			if(id in titles){
				entry.text(titles[id]);
			}else{
				$.get('https://forums.oneplus.net/threads/'+id, function(data){
					titles[id] = (data.match(/<div class="titleBar">\n<h1>(.*)<\/h1>/)[1]).replace(/&#039;/gi, "'");
					entry.text(titles[id]);
				});
			}
		};
		var reloadChecked = function(){
			getChecked();
			barSection.overlay.html('');
			for(a in checked){
				addOverlay(checked[a]);
			}
			if(checked.length == 0){
				barSection.overlay.append('<div class="modbar-overlay-section"><em>No Threads Selected</em></div>');
			}
		}
		barSection.btn.click(function(){
			reloadChecked();
		});
		var digest = function(){
			$('.InlineModChecked').removeClass('InlineModChecked');
			for(a in checkboxes){
				for(box in checkboxes[a]){
					checkboxes[a][box].prop('checked', false);
				}
			}
			for(a in checked){
				if(checked[a] in checkboxes){
					for(box in checkboxes[checked[a]]){
						checkboxes[checked[a]][box].prop('checked', true).closest('li').addClass('InlineModChecked');
					}
				}
			}
			barSectionInner.text('('+checked.length+')');
			docCookies.setItem('xf_inlinemod_threads', checked.join(','), false, '/');
		};
		var parseResponseHeaders = function(headerStr) {
			var headers = {};
			if (!headerStr) {
				return headers;
			}
			var headerPairs = headerStr.split('\u000d\u000a');
			for (var i = 0; i < headerPairs.length; i++) {
				var headerPair = headerPairs[i];
				// Can't use split() here because it does the wrong thing
				// if the header value has the string ": " in it.
				var index = headerPair.indexOf('\u003a\u0020');
				if (index > 0) {
					var key = headerPair.substring(0, index);
					var val = headerPair.substring(index + 2);
					headers[key] = val;
				}
			}
			return headers;
		}
		var getId = function(link, checkbox){
			$.ajax({
				url:link,
				success: function(data) {
					var id = Number(data.match(/\.(\d*)\/"><span class="DateTime"/)[1]);
					if(!isNaN(id)){
						checkbox.data('id', id);
						if(!(checkbox.data('id') in checkboxes)){
							checkboxes[checkbox.data('id')] = [];
						}
						checkboxes[checkbox.data('id')].push(checkbox);
						titles[id] = (data.match(/<div class="titleBar">\n<h1>(.*)<\/h1>/)[1]).replace(/&#039;/gi, "'");
						digest();
					}
				}
			});
			return 0;
		};
		getChecked();
		if(window.location.href.indexOf('forums.oneplus.net/search') != -1){
			$('.searchResultsList li').each(function(){
				var checkbox = $('<input type="checkbox" />');
				checkbox.data('id', 0);
				if($(this).attr('id').indexOf('post') == -1){
					getId('/threads/'+$(this).attr('id').replace('thread-',''), checkbox);
				}else{
					getId('/posts/'+$(this).attr('id').replace('post-',''), checkbox);
				}
				$(this).find('.title').prepend(checkbox);
				if(checkbox.data('id') != 0){
					if(!(checkbox.data('id') in checkboxes)){
						checkboxes[checkbox.data('id')] = [];
					}
					checkboxes[checkbox.data('id')].push(checkbox);
				}
				checkbox.after('&nbsp;');
				checkbox.change(function() {
					if($(this).is(":checked")) {
						if(checked.indexOf(checkbox.data('id')) == -1){
							checked.push(checkbox.data('id'));
						}
					}else{
						if(checked.indexOf(checkbox.data('id')) != -1){
							checked.splice(checked.indexOf(checkbox.data('id')), 1);
						}
					}      
					digest();
					if(barSection.overlay.is(':visible')){
						reloadChecked();
					}
				});
			});
			getChecked();
		}
		digest();
		window.setInterval(function(){
			var temp = checked;
			getChecked();
			if(checked != temp){
				digest();
				if(barSection.overlay.is(':visible')){
					reloadChecked();
				}
			}
		}, 5000);
	};
	var bar = $('<div id="modbar"><div class="pageWidth"></div><div style="clear:both;"></div></div>');
	bar.appendTo('body');
	var module = function(content, floatDir){
		this.btn = $('<div class="modbar-btn"></div>');
		this.btn.css('float', floatDir);
		bar.find('.pageWidth').append(this.btn);
		this.btn.append(content);
		this.overlay = $('<div class="modbar-overlay"></div>');
		this.overlay.appendTo('body').hide();
		var that = this;
		this.btn.click(function(){
			if(that.overlay.html() != ''){
				that.overlay.css({bottom: bar.height()});
				if(floatDir == 'right'){
					that.overlay.css({right: $(window).width() - that.btn.offset().left - that.btn.outerWidth()});
				}else{
					that.overlay.css({left: that.btn.offset().left});
				}
				that.overlay.toggle();
				if(that.overlay.is(':visible')){
					that.btn.addClass('active');
				}else{
					that.btn.removeClass('active');
				}
			}
		});
	};
	selectedThreads();
	if(window.location.href.indexOf('forums.oneplus.net/threads') != -1){
		var closeThreadLink = new module("Close Thread");
		var closeThreadOptions = $('<select style="width:300px;display:block;margin:2px 0px 10px 0px;"><option value="2">Duplicate</option><option value="3">Scalpers</option><option value="4">Contest over</option></select>');
		var closeThreadCustom = $('<textarea style="width:300px;height:100px;display:block;margin:2px 0px 10px 0px;"></textarea>');
		var closeThreadBtn = $('<button href="javascript:void(0);" class="button primary">Close Thread</button>');
		closeThreadLink.overlay.append('<strong style="font-weight:bold;">Close Reason:</strong><br/>').append(closeThreadOptions).append('<strong style="font-weight:bold;">Extra Notes:</strong><br/>').append(closeThreadCustom).append(closeThreadBtn).wrapInner('<div style="padding:20px;"></div>');
		closeThreadBtn.click(function(){
			closeThread(false, Number(closeThreadOptions.val()), $.trim(closeThreadCustom.val()));
		});
	}
    /*var batchClose = new module("Batch Close <span style='font-size:10px;'>(Use Responsibly!!!)</span>");
	batchClose.btn.click(function(){
		closeThread(true);
	});*/
    var modSubforumLink = new module("<a href=\"https://forums.oneplus.net/forums/moderation_team/\">Mod Subforum</a>");
    
    if(window.location.href.indexOf("thread") == -1 && window.location.href.indexOf("conversation") == -1){
                fMTUpdate();
    }
}
var $ = jQuery;
addJQuery(main);